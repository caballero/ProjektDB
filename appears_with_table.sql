CREATE TABLE appears_with(
	text_id INT NOT NULL,
    hashtag_id_1 INT NOT NULL,
    hashtag_id_2 INT NOT NULL,
    count INT,
    FOREIGN KEY (text_id) REFERENCES text (text_id),
    FOREIGN KEY (hashtag_id_1) REFERENCES hashtag (hashtag_id),
    FOREIGN KEY (hashtag_id_2) REFERENCES hashtag (hashtag_id)
)