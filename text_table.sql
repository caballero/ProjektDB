CREATE TABLE text(
    text_id INT NOT NULL,
    handle VARCHAR(15),
    original_author VARCHAR(15) NOT NULL,
    retweet_count INT,
    favorite_count INT,
    PRIMARY KEY (text_id)    
)