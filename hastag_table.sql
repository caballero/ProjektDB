CREATE TABLE Hashtag(
    text_id INT NOT NULL,
	hashtag_id INT NOT NULL,
    use_count INT,
	hashtag_name VARCHAR(150),
	time VARCHAR(150),
    PRIMARY KEY (hashtag_id),
    FOREIGN KEY (text_id) References text (text_id)
)