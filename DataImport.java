package aufgaben;


import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;


import com.opencsv.CSVReader;

public class DataImport {
	
	public Connection connection;	
	int id = 0;
	
	
	/*check if the values are integers*/
	
	public static boolean isNumeric(String str)
	{
		if(str == null || str == "") return false;
	    for (char c : str.toCharArray())
	    {
	        if (!Character.isDigit(c)) return false;
	    }
	    return true;
	}
	
	
	/*connect to database*/
	
	public Connection connect(){
		
		try {
			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("No driver found!");
			e.printStackTrace();
		}

		System.out.println("Driver found!");
		connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/Election", "postgres",
					"pandufla");

		} catch (SQLException e) {
			System.out.println("Connection Failed!");
			e.printStackTrace();
		
		}if (connection != null) {
			System.out.println("Connected!");
		} else {
			System.out.println("Failed to make connection!");
		}
		
		return connection;
	}
	
	/*read data from .csv file and import it into corresponding table*/
	
	public void importData(CSVReader reader, String sql, Connection con) throws NumberFormatException, IOException, SQLException{
		/*column represents the column number in the database-table*/
		int column = 1;
		/*index represents the current value in a row*/
		int index = 0;
		try { 	
			
		    String[] line = null; 
		      
		    while ((line = reader.readNext()) != null) {	        	
		            try {

		                if (line != null){
		                PreparedStatement ps =  con.prepareStatement(sql);
       
		                while(column < sql.length() && index < line.length){
		                	if(isNumeric(line[index])){
		                		
		                		/*exception for values: empty string etc...*/
		                		try{
		                			ps.setInt(column,Integer.parseInt(line[index]));
		                		}catch(NumberFormatException e){
		                			ps.setString(column, "");
		                		}
		                	}
		                	else{
		                		ps.setString(column, line[index]);
		                	}
		                	column++;
		                	index++;
		                }index = 0;
		                column = 1;
		                ps.executeUpdate();
				        ps.close();
  
		                    }		                 
		            }
		            finally
		            {
		            	//close data
		            }
		        }reader.close();
		    } catch (FileNotFoundException ex) {
		        ex.printStackTrace();
		    }
		
	}
	
	
	public static void main(String[] args) throws IOException, SQLException{
		
		/*Examples for the text and hashtag tables*/
		CSVReader text_data = null;
		CSVReader hashtag_data = null;
		
		
		/*data for text-table*/
		String sql1 = " INSERT INTO text(text_id,handle,original_author,retweet_count,favorite_count) VALUES(?,?,?,?,?) ";
		/*reading from the second line (in the first line are stored the headers of the columns)*/
		text_data = new CSVReader(new FileReader("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\cleanData.csv"),',','"',1);
		
		/*data for hashtag-table*/
		String sql2 = " INSERT INTO hashtag(text_id,hashtag_id, use_count, hashtag_name, time) VALUES(?,?,?,?,?) ";
		hashtag_data = new CSVReader(new FileReader("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\hashtagData.csv"),',','"',0);			
		
		DataImport dataImport = new DataImport();
		dataImport.importData(text_data, sql1, dataImport.connect());
		dataImport.importData(hashtag_data, sql2, dataImport.connect());
		
	}

}
