package aufgaben;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;


public class Cleaner {
	
	/**
	 reading from and writing to 
	 .csv files using the library opencsv	 	 
	
	**/

	
	public static void main(String[] args) throws Exception{
		
		/*rawData is the .csv file american-election-tweets.csv, from which we are going to read*/
		CSVReader rawData = null;
		/*in cleanData we are going to store the new cleaned .csv file*/
		CSVWriter cleanData =  null;
		/*in hashtagData we are going to store the data we need for the hashtag table*/
		CSVWriter hashtagData = null;
		/*primary keys for text-table and hashtag-table*/
		int text_id = 0;
		int hashtag_id = 0;
        
        try
        {
            rawData = new CSVReader(new FileReader("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\american-election-tweets.csv"),';','"',0);
            cleanData = new CSVWriter(new FileWriter("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\cleanData.csv")); 
            hashtagData = new CSVWriter(new FileWriter("C:\\Users\\Matea\\Documents\\Latex files\\Datenbanksysteme\\hashtagData.csv"));
            
            
            //tweet stores the values of the current row
            String[] tweet = null;
            //cleanedTweet stores only the values that we need (of the current row)
            ArrayList<String> cleanedTweet = new ArrayList<String>(); 
            //hashtag stores one hashtag at a time
            String hashtag = "";
 
            /*first read than write one row at a time while ignoring the columns we don't need*/ 
            while((tweet = rawData.readNext()) != null){
            	text_id++;
            	cleanedTweet.add(String.valueOf(text_id));
            	for(int i = 0; i < tweet.length; i++){
            		
            		/*columns: 0==handle, 3==original_author,7==retweet_count,8==favorite_count*/
            		if((i == 0 || i == 3 || i == 7 || i == 8)){
            			
            			cleanedTweet.add(tweet[i]);          			          			
            			
            		}
            		/*searching for the hashtags: column 1 is the text containing the hashtags*/
            		if(i==1){
            			for(int j = 0; j < tweet[1].length(); j++){          			
                 			if(tweet[1].charAt(j) == '#'){
                 				/*hashtags stores the all hashtags used in a row*/
                 				ArrayList<String> hashtags = new ArrayList<String>();
                 				hashtag_id++;
                 				int help = j+1;
                 					while(Character.isLetter(tweet[1].charAt(help)) && help < tweet[1].length()-1){
                 						hashtag += tweet[1].charAt(help);
                 					help++;
                 					}
                 					
                 					/*store text_id, hashtag_id, use_count, hashtag_name, timestamp*/
                 					hashtags.add(String.valueOf(text_id));
                 					hashtags.add(String.valueOf(hashtag_id));
                 					
                 					/*for testing we are passing 0 as use_count*/
                 					hashtags.add(String.valueOf(0));
                 					hashtags.add(hashtag);
                 					/*for testing we are passing the timestamp as string*/
                 					hashtags.add(tweet[4]);
                 					/*writing the hashtags to new hashtagData.csv file*/
                 					hashtagData.writeNext(hashtags.toArray(new String[hashtags.size()]));               					
                 					hashtag = "";				
                 			}
                 		}
            		}           		           		
           	 	}
            	/*writing the rows one by one*/
            	cleanData.writeNext(cleanedTweet.toArray(new String[cleanedTweet.size()]));
            	cleanedTweet.clear(); 
            
            }                                             
        }
        catch(Exception ee)
        {
            ee.printStackTrace();
        }
        finally
        {
		try
		{
			//closing the readers and writers
			rawData.close();
			cleanData.close();
			hashtagData.close();
			
			/*output: cleanData.csv containing all the attributes 
			 * (text_id, handle, original_author, retweet_count, favorive_count) we need for our text-table,
			 * hashtagData.csv containing all the attributes (text_id, hashtag_id, use_count, hashtag_name, timestamp)
			 * we need for our hashtag-table
			 * */
			
		}
		catch(Exception ee)
		{
			ee.printStackTrace();
		}
	}
    
	   }
	}
	
   