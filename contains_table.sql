CREATE TABLE contains(
	text_id INT NOT NULL,
    hashtag_id INT NOT NULL,
    FOREIGN KEY (text_id) REFERENCES text (text_id),
    FOREIGN KEY (hashtag_id) REFERENCES hashtag (hashtag_id)
)